#include "usr_general.h"
/* User variable decleration */
/* =======================================================================================================================*/
/* =======================================================================================================================*/
_io FATFS m_sFs;  /* f_mount first parameter  */
_io FATFS *m_pFs; /* f_getfree third parameter*/
_io FIL m_sFil;   /* f_open first parameter   */

/* SD card capacity related */
_io DWORD m_freeCluster;
_io uint32_t m_totalSpace;
_io uint32_t m_freeSpace;

/*error counter flag for SD CARD initial and file openning proccess*/
uint8_t m_errorCntMountingSD = 0;
uint8_t m_errorCntGetSpaceSD = 0;
uint8_t m_errorCntOpeningFileSD = 0;
uint8_t g_generalCnt = 0;

/* gets file size that we wrote */
uint32_t m_fileSize = 0;
FRESULT m_fresult;
char buf[64] = {0};
/* =======================================================================================================================*/
/* =======================================================================================================================*/

/**************************************************************************************************************************/
/*User function definitions*/
/**************************************************************************************************************************/
/*
 *   @definition : file is mounting, free place checking for SD Card
 *                 opening the file one times at the initial
 *   @param : none
 *   @ret : none
 */
void UsrLogInitial(void)
{
    /* mounting proccess */
    while (f_mount(&m_sFs, "/", 1) != FR_OK)
    {
        printf("[%d]FAIL in mounting\n", ++m_errorCntMountingSD);
        HAL_Delay(200);
        if (m_errorCntMountingSD >= 5)
        {
            m_errorCntMountingSD = 0;
            printf("f_mount error...System goes to RESET\n");
            UsrErrorLedBlink();
            HAL_NVIC_SystemReset();
            HAL_Delay(1000);
        }
    }
    // code will not get here unless f_mount return is success
    printf("**f_mount is ok**\n");

    /* checking free space in SD card*/
    while (f_getfree("1", &m_freeCluster, &m_pFs) != FR_OK)
    {
        printf("[%d]f_getfree function ERROR.No Free Space!\n", ++m_errorCntGetSpaceSD);
        HAL_Delay(200);

        if (m_errorCntGetSpaceSD >= 5)
        {
            m_errorCntGetSpaceSD = 0;
            printf("Getting free space ERROR...System goes to RESET\n");
            UsrErrorLedBlink();
            HAL_NVIC_SystemReset();
            HAL_Delay(1000);
        }
    }

    // code will not get here unless f_getfree return is success
    printf("****checking free space is successful****\n");
    m_totalSpace = (m_pFs->n_fatent - 2) * m_pFs->csize;
    m_freeSpace = m_freeCluster * m_pFs->csize;
    printf("%d KB total drive space.\n%d KB available.\n", m_totalSpace / 2, m_freeSpace / 2);
    // while (!UsrLogOpen())
    //     ;
    // printf("**f_open is ok**\n");

    // printf("wait for the system start....\n");

    // for (int i = 0; i < 3; ++i) {
    //     for (int k = 0; k < 3; ++k) {
    //         printf(" . ");
    //         HAL_Delay(50);
    //     }
    //     printf("\n");
    // }

    // char buf[64];
    // UINT br;
    // m_fresult = f_open(&m_sFil, "nezir.txt", FA_READ);
    // printf("m_result : %d\n", m_fresult);
    // m_fresult = f_lseek(&m_sFil, 0);
    // printf("m_result : %d\n", m_fresult);
    // m_fresult = f_read(&m_sFil, (void *)buf, 5, &br);
    // printf("m_result : %d\n", m_fresult);
    // //buf[5] = 0;
    // printf("buf : %s\n", buf);
    // f_close(&m_sFil);

    /***********************************************************************/
    /***********************************************************************/
    char buf[64];
    UINT br;
    m_fresult = f_open(&m_sFil, "nezir.txt", FA_CREATE_ALWAYS | FA_WRITE);
    printf("m_result : %d\n", m_fresult);
    //m_fresult = f_lseek(&m_sFil, 0);

    for (uint8_t i = 0; i < 10; ++i)
    {
        sprintf(buf, "deneme[%d]\n", i);
        f_puts((const TCHAR *)buf, &m_sFil);
    }
    f_close(&m_sFil);
    m_fresult = f_open(&m_sFil, "nezir.txt", FA_READ);

    while (f_gets((TCHAR *)buf, 63, &m_sFil) != NULL)
    {
        buf[63] = '\0';
        printf("string : %s", buf);
    }

    f_close(&m_sFil);
    /***********************************************************************/
    /***********************************************************************/

/****************************************************/
// nezir de çektik
    // //UINT br;

    // m_fresult = f_open(&m_sFil, "nezir.txt", FA_READ);
    // m_fresult = f_open(&m_sFil, "nezirwritten.txt", FA_CREATE_ALWAYS | FA_WRITE);

    // while (f_gets((TCHAR *)buf, 63, &m_sFil) != NULL)
    // {
    //     // buf[6] = '\0';
    //     //  printf("string : %s", buf);
    //     sprintf(buf, "%s\n", buf);

    //     m_fresult = f_lseek(&m_sFil, _size(buf));
    //     f_puts((const TCHAR *)buf, &m_sFil);
    //     f_close(&m_sFil);
    // }

    // f_close(&m_sFil);

/****************************************************/

}

/**************************************************************************************************************************/
/*
 *   @definition : it writes the sent data into SD card and
 *                 set the write pointer to the end of the file
 *   @param : it takes the parsed data from UART      @ref m_PackCollectBuf
 *   @ret : none
 */
void UsrLogGeneral(uint8_t *sentMatrixBuf)
{

    /* writing to the SD Card according to sentMatrixBuf set */
    UINT bw;
    if (f_write(&m_sFil, sentMatrixBuf, _size(sentMatrixBuf), &bw) != FR_OK)
    {
        UsrErrorLedBlink();
        HAL_NVIC_SystemReset();
    }
    _SYSTEM_DATA_TOGGLE_LED(); // GPIOD, 12th pin  whenever you write the buffer on SD Card
    m_fileSize += _size(sentMatrixBuf);
    f_lseek(&m_sFil, m_fileSize);
}

/**************************************************************************************************************************/
/*
 *   @definition : opens the file in append and write mode and
 *                  the f_lseek function moves write pointer to the end of the file
 *   @param : none
 *   @ret : success value
 *          return 1 : f_open success
 */
int UsrLogOpen(void)
{
    // while (f_open(&m_sFil, _USR_LOG_FILE_PATH, FA_OPEN_APPEND | FA_WRITE) != FR_OK)
    //     ;

    // m_fileSize = f_size(&m_sFil);
    // f_lseek(&m_sFil, m_fileSize);

    return 1;
}

/**************************************************************************************************************************/
/* @definition : The f_sync function performs the same process as f_close function but
                 the file is left opened and can continue read/write/seek operations to the file
   @param : address of file object
   @ret : enum type FRESULT
*/
void UsrLogClose(void)
{
    f_sync(&m_sFil);
}

/**************************************************************************************************************************/
/* @definition : when an error occured blink 12th pin of GPIOD
 *  @param : none
 *  @ret : none
 */
void UsrErrorLedBlink(void)
{
    for (int i = 0; i < 7; ++i)
    {
        _SYSTEM_LOG_ERROR_TOGGLE_LED();
        HAL_Delay(40);
    }
}
