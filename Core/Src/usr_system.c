
#include "usr_general.h"
#include <stdlib.h>
#include <math.h>
/* User variable decleration */
/* =======================================================================================================================*/
/* =======================================================================================================================*/

/* interrupt total receive buffer */
uint8_t m_receiveUartBuf[_USR_SYSTEM_LOG_UART_RAW_COLLECT_BUFER_SIZE];
/* Parsed date collects in this buffer */
_io uint8_t m_PackCollectBuf[_USR_PACKAGE_BUF_SIZE];
uint16_t m_OffSet = 0;
_iov uint8_t m_ItCameFlag = false;
_io uint16_t m_StartPoint = 0;
uint8_t m_info = 0;
_io void DataParseProc(void);
/* =======================================================================================================================*/
/* =======================================================================================================================*/

EBool UsrSdCardInitial(void);



FRESULT fr1, fr2;
FIL sFil1; /* f_open first parameter   */
FIL sFil2; /* f_open first parameter   */
char tempBuf[64];
char tempFilteredBuf[64];
_io FATFS m_hFile, *m_phFile;
double newValue;

// 1. Filtre için ara değerler
double a_vn = 0.;
double a_vn1 = 0.;
double a_vn2 = 0.;
// 2. Filtre için ara değerler
double b_vn = 0.;
double b_vn1 = 0.;
double b_vn2 = 0.;

//3. Filtre için ara değerler
double c_vn = 0.;
double c_vn1 = 0.;
double c_vn2 = 0.;

//4. Filtre için ara değerler
double d_vn = 0.;
double d_vn1 = 0.;
double d_vn2 = 0.;


#define _ITERATION_SIZE 1000
// 1. Filtre çıkışı
double filter_out = 0;
double filter_out2 = 0;
double filter_out3 = 0;
// 2. Filtre çıkışı
double y = 0;

double toplam = 0;
// 2. Dereceden Filtre Katsayıları
double b0 = 1.;
double b1 = 0.;
double b2 = -1.;
double a1 = -1.98323277169363665;
double a2 = 0.998995195336138008;
double K = 0.000502402331931055766;

void UsrSystemTestInitial(void)
{
    // UsrLogInitial();
    if (UsrSdCardInitial())
        printf("SD card system succesfully initialized\n");

    fr1 = f_open(&sFil1, "RawValues.txt", FA_READ | FA_OPEN_EXISTING);
    fr2 = f_open(&sFil2, "NewValues.txt", FA_CREATE_NEW | FA_WRITE);

    printf("f_opens results -->fr1: %d, fr2: %d\n", fr1, fr2);

    TCHAR *p;

    int i = 0;
    while (true)
    {
        _io EBool finishedFlg;

        if ((p = f_gets((TCHAR *)tempBuf, 64, &sFil1)) != NULL)
        {
            char *str = strchr(tempBuf, '\n');
            *str = '\0';
            newValue = atof((const char *)tempBuf);

            /* filtering process */
            a_vn = newValue - a1 * a_vn1 - a2 * a_vn2;
            filter_out = K * (b0 * a_vn + b1 * a_vn1 + b2 * a_vn2);
            a_vn2 = a_vn1;
            a_vn1 = a_vn;

            b_vn = filter_out - a1 * b_vn1 - a2 * b_vn2;
            y = K * (b0 * b_vn + b1 * b_vn1 + b2 * b_vn2);
            b_vn2 = b_vn1;
            b_vn1 = b_vn;

            /* additional filter */
            c_vn = y - a1 * c_vn1 - a2 * c_vn2;
            filter_out2 = K *(b0 * c_vn + b1 * c_vn1 + b2 * c_vn2);
            c_vn2 = c_vn1;
            c_vn1 = c_vn;

            d_vn = filter_out2 - a1 * d_vn1 - a2 * d_vn2;
            filter_out3 = K *(b0 * c_vn + b1 * d_vn1 + b2 * d_vn2);
            d_vn2 = d_vn1;
            d_vn1 = d_vn;
            /*********************/

            if ((20000 < i) && (i < 90000))
                toplam = toplam + (filter_out3 * filter_out3); // son filtreden çıkan degeri giriyoruz 
            ++i;

            sprintf(tempFilteredBuf, "%.8lf\n", filter_out3);
            f_puts((const TCHAR *)tempFilteredBuf, &sFil2);

            if (i >= _ITERATION_SIZE)
            {
                double RMS_Signal = sqrt(toplam / 70000.);
                printf("RMS Value : %.8lf\n", RMS_Signal);
                break;
            }
            printf("[%6d]%.8lf ===> %.8lf\n", i, newValue, filter_out3);
        }
        else
        {
            if (!finishedFlg)
            {
                // f_close(&sFil1);
                f_close(&sFil2);

                printf("file write finished\n");

                finishedFlg = true;
            }

            printf("f_gets error\n");
        }

        HAL_Delay(1);
    }

    f_close(&sFil1);
    f_close(&sFil2);
    
    
    int x = 5;
    while (x--)
        printf("Your processed is finished!\n");
}

EBool UsrSdCardInitial(void)
{

    uint8_t m_errMountCnt = 0;

    /* mounting proccess */
    while (f_mount(&m_hFile, "/", 1) != FR_OK)
    {
        printf("[%d]FAIL in mounting\n", ++m_errMountCnt);
        HAL_Delay(200);

        if (m_errMountCnt >= 5)
        {
            m_errMountCnt = 0;
            printf("f_mount error...System goes to RESET\n");
            UsrErrorLedBlink();
            HAL_NVIC_SystemReset();
            HAL_Delay(1000);
        }
    }

    printf("Mount process succesfully finished\n");

    DWORD freeCluster;
    uint8_t m_errorCntGetSpaceSD = 0;

    /* checking free space in SD card*/
    while (f_getfree("1", &freeCluster, &m_phFile) != FR_OK)
    {
        printf("[%d]f_getfree function ERROR.No Free Space!\n", ++m_errorCntGetSpaceSD);
        HAL_Delay(200);

        if (m_errorCntGetSpaceSD >= 5)
        {
            m_errorCntGetSpaceSD = 0;
            printf("Getting free space ERROR...System goes to RESET\n");
            UsrErrorLedBlink();
            HAL_NVIC_SystemReset();
            HAL_Delay(1000);
        }
    }

    // code will not get here unless f_getfree return is success
    printf("****checking free space is successful****\n");

    uint32_t totalSpaceSize;
    uint32_t freeSpaceSize;

    totalSpaceSize = (m_phFile->n_fatent - 2) * m_phFile->csize;
    freeSpaceSize = freeCluster * m_phFile->csize;
    printf("%d KB total drive space.\n"
           "%d KB available.\n",
           totalSpaceSize / 2, freeSpaceSize / 2);

    return true;
}

void UsrSystemTestGeneral(void)
{
}





/*************************************** USELESS ***************************************/
/**************************************************************************************************************************/
/*  @definition : It initials timer, required SD card initials and Receive interrupt first time
 *  @param : none
 *  @ret : none
 */
void UsrSytemInitial(void)
{
    HAL_TIM_Base_Start_IT(&htim6);
    /* SD card initializing*/
    UsrLogInitial();

    /* Set receive interrupt */
    HAL_UART_Receive_IT(&huart2, &m_receiveUartBuf[m_OffSet], 32);
    HAL_Delay(250);
}

/**************************************************************************************************************************/
/*
 * whenever interrupt occurs, check the off set value if it exceed the total buffer size
 *  if there is no problem, set the interrupt flag and set up the new interrupt according to off set value
 */
void UsrSystemCallback(void)
{

    m_OffSet += _USR_OFFSET_VALUE;                               /* setting m_OffSet for its new value */
    if (m_OffSet >= _USR_SYSTEM_LOG_UART_RAW_COLLECT_BUFER_SIZE) /* checking offset value exceeds the buf size */
    {
        m_OffSet = 0;
        m_StartPoint = 0;
    }

    m_ItCameFlag = true; /* setting interrupt came flag */

    HAL_UART_Receive_IT(&huart2, &m_receiveUartBuf[m_OffSet], 32); /* set up the new interrupt according to new off set value */
}

/**************************************************************************************************************************/
/*
 *   @definitions : it always works in the main while loop and listens whether UART receive interrupt came or not
 *                  if interrupt occurs, it parses the data according to data frame by using DataParseProc function
 *   @param : none
 *   @ret : none
 */
void UsrSytemGeneral(void)
{
    // DataParseProc();
}

/**************************************************************************************************************************/
/*DATA PARSING ACCORDING TO GIVEN MODE*/
