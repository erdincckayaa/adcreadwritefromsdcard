#ifndef __USR_GENERAL_H
#define __USR_GENERAL_H

#include "main.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"
#include <string.h>
#include <stdio.h>
#include "fatfs_sd.h"
#include "fatfs.h"
#include "tim.h"

/* generic defines*/
#define _io static
#define _iov static volatile

/**
 * @ref _USR_GENERAL_MODES :
 *
 * 0 :
 *  gets the received data until it come across '\n' whis is stop token
 *  if the package just contains of '\n' it returns "empty package" array
 *
 * 1 :
 *   @dataFrame below :
 *   Start Token     => 0x1A
 *   Data Size       => 0x???? // 2byte
 *   Data Info       => 0x00 : Info
 *                      0x01 : Warning
 *                      0x02 : Error
 *
 *   Data Command    => 0x00 : Enum
 *                   => 0x01 : String
 *   After Data Command (size - 2) bytes data you need to get
 *

 *
 * //we are checking sent logs from ESP32
 * 3 :
 *  gets the received data until it come across '\r','\n' in a row which are stop tokens
 *
 * //we are checking created logs from ESP32
 * 2 :
 *  *   @dataFrame below :
 *   Start Token     => 0x1A
 *   Data Size       => 0x?? // 1Byte
 *   Data Info       => 0x00 : Info
 *                      0x01 : Warning
 *                      0x02 : Error
 *
 *   Data Command    => 0x00 : Enum
 *                   => 0x01 : String
 *   After Data Command (size - 2) bytes data you need to get
 *
 */

/*gets length of buffer*/
#define _size(x) strlen((const char *)x)

/* generic bool enum */
typedef enum
{
    false,
    true
} EBool;

/* generic converter union define */
typedef union U_CONVERTER_TAG
{
    uint8_t buf[4];
    uint32_t ui32val;
    int32_t i32val;
    uint16_t ui16val;
    int16_t i16val;
} U_CONVERTER;

typedef enum
{
    ENUM,
    STRING
} ECommand;

/* @ref _USR_GENERAL_MODES */
#define _USR_GENERAL_MODE 0xFF

#if _USR_GENERAL_MODE == 0
#define _USR_GENERAL_STOP_TOKEN '\n'

#elif _USR_GENERAL_MODE == 1 /* 2 byte length */
#define _USR_GENERAL_START_TOKEN 0x1A

#elif _USR_GENERAL_MODE == 2 /* 1 byte length */
#define _USR_GENERAL_START_TOKEN 0x1A

#elif _USR_GENERAL_MODE == 3
#define _USR_GENERAL_STOP_TOKEN_1 '\r'
#define _USR_GENERAL_STOP_TOKEN_2 '\n'

#endif

/*we include all the headers that we create */
#include "usr_system.h"
#include "usr_log.h"

#endif //__USR_GENERAL_H
