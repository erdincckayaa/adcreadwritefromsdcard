#ifndef __USR_LOG_H
#define __USR_LOG_H


#define _USR_LOG_FILE_BUF_LEN 1024

/* picking the file name according to chosen _USR_GENERAL modes*/
#if _USR_GENERAL_MODE == 0
#define _USR_LOG_FILE_PATH "LogMode1.txt"
#elif _USR_GENERAL_MODE == 1
#define _USR_LOG_FILE_PATH "LogMode2.txt"
#elif _USR_GENERAL_MODE == 2
#define _USR_LOG_FILE_PATH "LogMode3.txt"
#elif _USR_GENERAL_MODE == 3
#define _USR_LOG_FILE_PATH "LogMode4.txt"

#endif

/*Led ON/OFF defines*/
#define _SYSTEM_LOG_ERROR_LED_Pin GPIO_PIN_12
#define _SYSTEM_LOG_ERROR_LED_GPIO_Port GPIOD
#define _SYSTEM_LOG_ERROR_TOGGLE_LED() (_SYSTEM_LOG_ERROR_LED_GPIO_Port->ODR ^= _SYSTEM_LOG_ERROR_LED_Pin)

void UsrLogInitial(void);
void UsrLogGeneral(uint8_t *m_fileBuff);
void UsrErrorLedBlink(void);
int UsrLogOpen(void);
void UsrLogClose(void);


#endif //__USR_LOG_H
