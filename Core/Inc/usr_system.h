#ifndef __USR_SYSTEM_H
#define __USR_SYSTEM_H

#define _USR_SYSTEM_LOG_UART_TRANSMIT_CHANNEL huart4
#define _USR_SYSTEM_LOG_UART_CHANNEL huart2
#define _USR_SYSTEM_LOG_UART_RAW_CHANNEL USART2

#define _USR_SYSTEM_LOG_UART_RAW_COLLECT_BUFER_SIZE 2048
#define _USR_PACKAGE_BUF_SIZE   512

#define _USR_OFFSET_VALUE   32


// #define _SYSTEM_STATUS_LED(x) (x ? (_SYSTEM_STATUS_LED_GPIO_Port->BSRR = _SYSTEM_STATUS_LED_Pin) : (_SYSTEM_STATUS_LED_GPIO_Port->BRR = _SYSTEM_STATUS_LED_Pin))
// #define _STATUS_DATA_LED(x) (x ? (_DATA_PROCCESS_LED_GPIO_Port->BSRR = _DATA_PROCCESS_LED_Pin) : (_DATA_PROCCESS_LED_GPIO_Port->BRR = _DATA_PROCCESS_LED_Pin))

/* Led toggle pins to see some system tasks whether it works */
#define _SYSTEM_STATUS_TOGGLE_LED() (_SYSTEM_STATUS_LED_GPIO_Port->ODR ^= _SYSTEM_STATUS_LED_Pin)
#define _SYSTEM_DATA_TOGGLE_LED() (_DATA_PROCCESS_LED_GPIO_Port->ODR ^= _DATA_PROCCESS_LED_Pin)

extern uint8_t m_receiveUartBuf[_USR_SYSTEM_LOG_UART_RAW_COLLECT_BUFER_SIZE];

void UsrSytemInitial(void);
void UsrSytemGeneral(void);
void UsrSystemCallback(void);
int getStringIndexProc(const int *pIndexArr, uint16_t enumValue);
void UsrSystemTestInitial(void);
void UsrSystemTestGeneral(void);

#endif //__USR_SYSTEM_H
